<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSdnCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdn_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('region');
            $table->string('district');
            $table->string('suburb');
            $table->string('SDN');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('sdn_codes');
    }
}
