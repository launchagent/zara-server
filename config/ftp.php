<?php
return array(

    /*
	|--------------------------------------------------------------------------
	| Default FTP Connection Name
	|--------------------------------------------------------------------------
	|
	| Here you may specify which of the FTP connections below you wish
	| to use as your default connection for all ftp work.
	|
	*/

    'default' => 'connection1',

    /*
    |--------------------------------------------------------------------------
    | FTP Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the FTP connections setup for your application.
    |
    */

    'connections' => array(

        'connection1' => array(
            'host'   => 'ftpagent.realestate.co.nz',
            'port'  => 21,
            'username' => 'LATRINITY1',
            'password'   => 'LATRINITY1',
            'passive'   => false,
        ),
        'connection2' => array(
            'host'   => '162.243.28.231',
            'port'  => 21,
            'username' => 'ftptest',
            'password'   => 'faith98',
            'passive'   => false,
        ),
    ),
);