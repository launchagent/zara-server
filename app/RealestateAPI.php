<?php

namespace ZaraServer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class RealestateAPI extends Model
{
  
  // 'sandbox' or 'live'
  private $environment     = 'sandbox'; 
  public $url;
  public $username = '';
  public $password = '';
  public $publicKey = 'C3BF2ACC-8A96-42AD-AE66-F23BF7AF1276';
  public $privateKey = '02B4C741-8263-4CBC-BA68-3675F2E08379';                            

	public function __construct() {
        if(App::environment('production')) {
            $this->url = 'https://api.realestate.co.nz/1/'; 
        } else {
            $this->url = 'https://api.realestate.co.nz/1/';  
        }
	}

    private function build_signature($endpoint, $params = array(), $useAuth = false) {
        $signature = strtolower($this->privateKey) . '/1/' . $endpoint;
        if($useAuth) {
            $signature .= $this->username . $this->password;
        }
        if(count($params) > 0) {
            $paramString = '';
            ksort($params);
            foreach($params as $key => $val) {
                $paramString .= $key . $val;
            }
            $signature .= $paramString;
        }
        return md5( $signature );
    }

    private function build_request_url($endpoint, $signature, $params = array()) {
        $j = (count($params) === 0) ? '?' : '&';
        return $this->url . $endpoint . $this->request_params($params) . $j . 'api_key=' . $this->publicKey . '&api_sig=' . $signature;
    }

    private function request_params($params) {
        if(count($params) > 0) {
            $paramString = '';
            ksort($params);
            $c = 0;
            foreach($params as $key => $val) {
                $j = ($c === 0) ? '?' : '&';
                $paramString .= $j . $key . '=' . $val;
                $c++;
            }
            return $paramString;
        } else {
            return '';
        }
    }

    public function perform_http_request($endpoint, $Method = 'GET', $params = array(),  $fields = NULL)
    { 
      $signature = $this->build_signature($endpoint, $params);

      $URL = $this->build_request_url($endpoint, $signature, $params); 
      $curl = curl_init($URL);  

      $header = array();
  		//$header[] = 'Content-length: 0';
      if($Method == 'GET') {
        $header[] = 'Content-type: application/json';
      } else {
        $header[] = 'Content-type: application/x-www-form-urlencoded; charset=UTF-8';
      }
      
      curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT        => 30,
       // CURLOPT_USERAGENT      => 'OAuth (' . $_SERVER['HTTP_HOST'] . ')',
        CURLOPT_HEADER         => FALSE,
        CURLOPT_HTTPHEADER     => $header,
        CURLINFO_HEADER_OUT    => TRUE
      ));

      
      // Do anything for the specific method
      switch(strtoupper($Method))
      {
        case 'GET':
        {        
          
        }
        break;
        
        case 'POST':
        { 
          $fieldsString = '';
          //url-ify the data for the POST
          foreach($fields as $key=>$value) { 
            $value = (is_array($value)) ? json_encode($value) : $value;

            $fieldsString .= $key.'=' . $value .'&'; 
          }
          rtrim($fieldsString, '&');

          curl_setopt_array($curl, array(
            CURLOPT_POST => count($fields),
            CURLOPT_POSTFIELDS => $fieldsString
          ));
        }
        break;
        
        case 'DELETE':
        {
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }         
        break;
      }
            
      // Do the request and grab the response and some info about it which may be useful
      $response = curl_exec($curl);
      $info     = curl_getinfo($curl);
      if(!defined('CURLINFO_HEADER_OUT'))
      {
        $info['request_header'] = implode("\r\n", $Headers);
      }
      
      // Ditch curl and return the response and info
      curl_close($curl);      
      return array($response);
    }

}
