<?php

namespace ZaraServer;

use Illuminate\Database\Eloquent\Model;

class SDNCode extends Model
{
    //
    protected $table = 'sdn_codes';
    public $timestamps = false;
    
}
