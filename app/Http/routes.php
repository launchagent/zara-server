<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'api/v1'], function()
{
	Route::get('/place/{placeid}', 'GooglePlaceController@get');
	Route::post('/place', 'GooglePlaceController@autocomplete');

	Route::post('/upload/profile', 'UploadController@uploadProfileImage');
	Route::post('/upload/property', 'UploadController@uploadPropertyImage');
	Route::post('/upload/gallery', 'UploadController@uploadGalleryImages');
	Route::post('/upload/file', 'UploadController@uploadFile');

	// Trade Me routes
	Route::get('/trademe/listing/get/{id}', 'TradeMeController@getListing');
	Route::post('/trademe/listing', 'TradeMeController@addListing');
	Route::post('/trademe/listing/edit', 'TradeMeController@editListing');
	Route::post('/trademe/listing/fees', 'TradeMeController@getAddListingFees');	
	Route::post('/trademe/listing/withdraw', 'TradeMeController@withdrawListing');
	Route::post('/trademe/listing/boost', 'TradeMeController@boostListing');
	Route::get('/trademe/photo/delete/{id}', 'TradeMeController@removePhoto');
	Route::post('/trademe/photos/add', 'TradeMeController@addPhoto');
	Route::post('/trademe/photos/agent', 'TradeMeController@addAgentPhoto');
	Route::get('/trademe/photos', 'TradeMeController@listPhotos');	
	Route::get('/localities', 'TradeMeController@getLocalities');
	Route::get('/categories', 'TradeMeController@listCategories');

	// Realestate routes
	Route::get('/realestate/regions', 'RealEstateController@regions');
	Route::get('/realestate/districts/{region}', 'RealEstateController@districts');
	Route::get('/realestate/suburbs/{region}/{district}', 'RealEstateController@suburbs');
	Route::get('/realestate/test', 'RealEstateController@test');

	Route::post('files/delete', 'FileController@deleteFiles');

	// Website pass through route
	// We have to do it this way rather than direct to the website because of CORS issues with the OPTIONS request
	Route::post('/website', 'WebsiteController@index');
});	

// Route::get('images/profile/{filename}', 'FileController@showProfileImage');
// Route::get('images/property/{filename}', 'FileController@showPropertyImage');
// Route::get('images/property/gallery/{filename}', 'FileController@showGalleryImage');

// Route::get('files/{id}/{filename}', 'FileController@showFile');