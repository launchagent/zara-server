<?php

namespace ZaraServer\Http\Controllers;

use Illuminate\Http\Request;

use ZaraServer\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;

class UploadController extends Controller
{
	public $imagesPath = '/public/images/';
    public $filesPath = '/public/files/';
	public $smallWidth = 400;
	public $mediumWidth = 900;
	public $largeWidth = 1600;

    //
    public function index(Request $request) {
    	return response()->json(['test' => 'this is a test']);
    }

    public function getSizeWidth($size) {

    	switch ($size) {
    		case 'lg':
    			$width = $this->largeWidth;
    			break;
    		case 'md':
    			$width = $this->mediumWidth;
    			break;
    		default:
    			$width = $this->smallWidth;
    			break;
    	}
    	return $width;
    }

    public function saveFile($path, $size, $extension, $file) {
    	$savePath = $this->imagesPath . $path . '_' . $size . $extension;
        $returnPath = $path . '_' . $size . $extension;
    	Storage::put($savePath, file_get_contents($file));
		$img = Image::make(storage_path() . '/app' . $savePath);

		$img->resize($this->getSizeWidth($size), null, function ($constraint) {
		    $constraint->aspectRatio();
		});

		$img->save(storage_path() . '/app' . $savePath);    	
    	return $returnPath;
    }

	public function uploadImage($request, $to = '') {
 
		$files = $request->file('uploads');
		$id = $request->input('id');
        $index = ($request->input('index')) ? $request->input('index') : 0;
        $filePaths = array();

    	if(!empty($files)):

    		foreach($files as $file):
                $timestamp = date('YmdHis');
                $indexMark = ($index === 0) ? '' : '_' . $index++;
    			$extension = '.' . $file->getClientOriginalExtension();
    			$filePaths[] = array('large' => $this->saveFile($to . '/' . $id . '_' . $timestamp . $indexMark, 'lg', $extension, $file));
    			$filePaths[] = array('medium' => $this->saveFile($to . '/' . $id . '_' . $timestamp . $indexMark, 'md', $extension, $file));
    			$filePaths[] = array('small' => $this->saveFile($to . '/' . $id . '_' . $timestamp . $indexMark, 'sm', $extension, $file));
    		endforeach;

    	endif;

		return response()->json(['files' => $filePaths]);
		
	}

	public function uploadProfileImage(Request $request) {
		return $this->uploadImage($request, 'profile');
	}

	public function uploadPropertyImage(Request $request) {
		return $this->uploadImage($request, 'property');
	}

    public function uploadGalleryImages(Request $request) {
        return $this->uploadImage($request, 'property/gallery');
    }

    public function uploadFile(Request $request) {
 
        $files = $request->file('uploads');
        $id = $request->input('id');
        $index = ($request->input('index')) ? $request->input('index') : 0;
        $filePaths = array();

        if(!empty($files)):

            foreach($files as $file):
                $indexMark = ($index === 0) ? '' : $index++ . '_';
                $extension = $file->getClientOriginalExtension();
                $fileName = $file->getClientOriginalName();
                
                $savePath = $this->filesPath . '/' . $id . '/' . $indexMark . $fileName;
                $returnPath = $indexMark . $fileName;
                Storage::put($savePath, file_get_contents($file));
                $filePaths[] = array('id' => $id, 'name' => $returnPath, 'extension' => $extension);

            endforeach;

        endif;

        return response()->json(['files' => $filePaths]);
        
    }

}
