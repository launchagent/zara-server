<?php

namespace ZaraServer\Http\Controllers;

use Illuminate\Http\Request;

use ZaraServer\Http\Requests;

class GooglePlaceController extends Controller
{
    //

    function autocomplete(Request $request) {

        if(!empty($request->input('input'))) {
            $response = \GoogleMaps::load('placeautocomplete')
            ->setParam ([
            'input'    => $request->input('input'),
            'location' => [
                'lat'  => $request->input('lat'),
                'lng'  => $request->input('long')
                ],
             'radius' => $request->input('radius') 
            ])
            ->get();
            $res = json_decode($response);
            $res = (!empty($res->predictions)) ? $res->predictions : array();
            return response()->json($res);      
        } else {
            return response(400);
        } 
    }

    // Get the details of a place
    function get(Request $request) {

        if(!empty($request->placeid)) {
            $response = \GoogleMaps::load('placedetails')
            ->setParam ([
            'placeid'    => $request->placeid
            ])
            ->get();
            $res = json_decode($response);
            return response()->json($res);      
        } else {
            return response(400);
        } 
    }
}
