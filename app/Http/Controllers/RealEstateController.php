<?php

namespace ZaraServer\Http\Controllers;

use Illuminate\Http\Request;
use ZaraServer\Http\Requests;
use ZaraServer\RealestateAPI;
use ZaraServer\Website;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Nathanmac\Utilities\Parser\Facades\Parser;
use ZaraServer\SDNCode;

class RealEstateController extends Controller

{
    public $SDNCode;


    public function __construct(SDNCode $SDNCode) {
        $this->SDNCode = $SDNCode;
    }

    //
    public function regions() {
        $results = $this->SDNCode->distinct()->orderBy('region', 'asc')->get(['region']);
    	return response()->json(['response' => $results]);
    }

    public function districts($region) {
        $results = $this->SDNCode->where('region', $region)->distinct()->orderBy('district', 'asc')->get(['district']);
        return response()->json(['response' => $results]);
    }

    public function suburbs($region, $district) {
        $results = $this->SDNCode->where('region', $region)->where('district', $district)->orderBy('suburb', 'asc')->get(['suburb', 'SDN']);

        return response()->json(['response' => $results]);
    }

    public function test(RealestateAPI $realestate) {
        $results = $realestate->perform_http_request('listings/', 'GET', array('listing_no' => 'TTN136055'));
        return response()->json(['response' => $results]);
    }

}
