<?php

namespace ZaraServer\Http\Controllers;

use Illuminate\Http\Request;

use ZaraServer\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;

class FileController extends Controller
{
    public $imagesPath = '/app/public/images/';
    public $filesPath = '/app/public/files/';

    //
    public function index(Request $request) {
    	return response()->json(['test' => 'this is a test']);
    }

    public function showImage($from, $filename) {

        $path = storage_path() . $this->imagesPath . $from . '/' . $filename;

        if(!File::exists($path)) abort(404);

        $file = File::get($path);
        $type = File::mimeType($path);

        $res = response()->make($file, 200);
        $res->header("Content-Type", $type);

        return $res;
    }

    public function showProfileImage($filename){
        return $this->showImage('profile', $filename);
    }

    public function showPropertyImage($filename){
        return $this->showImage('property', $filename);
    }

    public function showGalleryImage($filename){
        return $this->showImage('property/gallery', $filename);
    }

    public function showFile($id, $filename) {

        $path = storage_path() . $this->filesPath . $id . '/' . $filename;

        if(!File::exists($path)) abort(404);

        $file = File::get($path);
        $type = File::mimeType($path);

        $res = response()->make($file, 200);
        $res->header("Content-Type", $type);

        return $res;
    }

    public function deleteFiles(Request $request) {
        $data = $request->all();
        $num = 0;
        if(isset($data['files']) && isset($data['type'])) {
            foreach ($data['files'] as $source) {
                $path = ($data['type'] == 'images') ? storage_path() . $this->imagesPath . $source : storage_path() . $this->filesPath . $source;
                if(File::exists($path)) {
                   File::delete($path);
                    $num++; 
                }
            }
        } 
        return response()->json(array('response' => 'deleted ' . $num . ' files'));
    }


}
