<?php

namespace ZaraServer\Http\Controllers;

use Illuminate\Http\Request;

use ZaraServer\Http\Requests;
use ZaraServer\TradeMe;
use ZaraServer\Website;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;
use Nathanmac\Utilities\Parser\Facades\Parser;


class TradeMeController extends Controller

{


    public function getListing(TradeMe $tradeMe, $id) {

            $res = $tradeMe->perform_http_request('/Listings/' . $id . '.xml', 'GET');
            return response()->json(['response' => $res]);
    }

    public function editListing(Request $request, TradeMe $tradeMe) {
        $data = $request->all();
        if(!empty($data['trademeListingID'])) {
            $res = $tradeMe->perform_http_request('/Selling/Edit.xml', 'POST', $tradeMe->edit_listing_data($data));
            $listing = Parser::xml($res[0]);

            // $savePath = '/public/listings/test.xml';
            // $listing = $tradeMe->edit_listing_data($data);
            // Storage::put($savePath, $listing);
            
            return response()->json(['response' => $listing]);
        } else {
            return response()->json(['response' => array('error' => 'no listing id supplied')]);
        }
    }

    public function addListing(Request $request, TradeMe $tradeMe) {
        $data = $request->all();
        if($data['trademeListingID'] == '') {
            $res = $tradeMe->perform_http_request('/Selling.xml', 'POST', $tradeMe->create_listing_data($data));
            $listing = Parser::xml($res[0]); 

            //$listing = $tradeMe->create_listing_data($data);
           return response()->json(['response' => $listing]);     
        } else {
            $listing = $tradeMe->create_listing_data($data);
            return response()->json(['response' => $listing]); 
        }
    }

    public function getAddListingFees(Request $request, TradeMe $tradeMe) {
        $data = $request->all();
         $res = $tradeMe->perform_http_request('/Selling/Fees.xml', 'POST', $tradeMe->create_listing_data($data));
         $listing = Parser::xml($res[0]); 
        //$listing = $tradeMe->create_listing_data($data);
       return response()->json(['response' => $listing]);     
    }

    public function withdrawListing(Request $request, TradeMe $tradeMe) {
        $data = $request->all();
        $res = $tradeMe->perform_http_request('/Selling/Withdraw.xml', 'POST', $tradeMe->withdraw_listing_data($data));
        $listing = Parser::xml($res[0]); 
        return response()->json(['response' => $listing]);
    }

    public function boostListing(Request $request, TradeMe $tradeMe) {
        $data = $request->all();
        $res = $tradeMe->perform_http_request('/Selling/ListingTemporalExtraPromotion.xml', 'POST', $tradeMe->boost_listing_data($data));
        $listing = Parser::xml($res[0]); 
        return response()->json(['response' => $listing]);
    }

    public function addPhoto(Request $request, TradeMe $tradeMe) {
        $photoData = $request->all();
        $img = File::get(storage_path() . '/app/public/images/' . $photoData['photo']);
        $res = $tradeMe->perform_http_request('/Photos/Add.xml', 'POST', $tradeMe->create_photo_data($img));
        $photos = Parser::xml($res[0]);
        if(isset($photoData['ID'])) {
            $photos['ID'] = $photoData['ID'];
        }
        return response()->json(['response' => $photos]);
    }

    public function addAgentPhoto(Request $request, TradeMe $tradeMe) {
        $photoData = $request->all();
        $img = File::get(storage_path() . '/app/public/images/' . $photoData['photo']);
        $res = $tradeMe->perform_http_request('/Photos/agentbranding.xml', 'POST', $tradeMe->create_photo_data($img));
        $photos = Parser::xml($res[0]);
        if(isset($photoData['ID'])) {
            $photos['ID'] = $photoData['ID'];
        }
        return response()->json(['response' => $photos]);
    }

    public function removePhoto(TradeMe $tradeMe, $id) {
        $res = $tradeMe->perform_http_request('/Photos/' . $id . '/Remove.xml', 'DELETE');
        $photos = Parser::xml($res[0]);
        return response()->json(['response' => $photos]);
    }

    public function listPhotos(TradeMe $tradeMe) {
        $res = $tradeMe->perform_http_request('/Photos.json', 'GET');
        return response()->json(['response' => $res]);
    }

    public function getLocalities(TradeMe $tradeMe) {
        $res = $tradeMe->perform_http_request('/Localities.json');
        return response()->json(['response' => $res[0]]);   
    }

    public function listCategories(TradeMe $tradeMe) {
        $res = $tradeMe->perform_http_request('/Categories/3399/Details.json', 'GET');
        return response()->json(['response' => $res]);
    }

}
