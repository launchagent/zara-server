<?php

namespace ZaraServer\Http\Controllers;

use Illuminate\Http\Request;

use ZaraServer\Http\Requests;

class WebsiteController extends Controller
{
    //
    public function index(Request $request) {

     $data = $request->all();   
     $ch = curl_init($data['url']);  
     $header = array();   
     $body = json_encode($data['data']);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);                                                                  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
        'Content-Type: application/json',                                                                                
        'Content-Length: ' . strlen($body))                                                                       
    ); 

      // Do the request and grab the response and some info about it which may be useful
      $response = curl_exec($ch);
      $info     = curl_getinfo($ch);
      if(!defined('CURLINFO_HEADER_OUT'))
      {
        $info['request_header'] = implode("\r\n", $Headers);
      }
      
      // Ditch curl and return the response and info
      curl_close($ch);
      return response($response);
    }
}
