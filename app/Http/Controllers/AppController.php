<?php

namespace ZaraServer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use ZaraServer\Http\Requests;
use ZaraServer\Website;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;
use Nathanmac\Utilities\Parser\Facades\Parser;


class AppController extends Controller

{
    //
    public function index(Request $request) {

    	return response()->json(['test' => App::environment('local')]);
    }

}
