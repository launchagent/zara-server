<?php

namespace ZaraServer\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Realestate::class,
        Commands\RealestateListingDetails::class,
        Commands\RealestateOpenhomes::class,
        Commands\TrademeAutoextend::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('zara:realestate')
                ->dailyAt('11:00');

        $schedule->command('zara:realestateopenhomes')
                ->hourly();         

        $schedule->command('zara:realestatelistingdetails')
                ->hourly();  

        $schedule->command('zara:trademeautoextend')
                ->hourly();                
    }
}
