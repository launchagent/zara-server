<?php

namespace ZaraServer\Console\Commands;

use Illuminate\Support\Facades\App;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Nathanmac\Utilities\Parser\Facades\Parser;
use Firebase\FirebaseLib;
use Anchu\Ftp\Facades\Ftp as FTP;
use ZaraServer\SDNCode;

class Realestate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zara:realestate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This runs the integration for realestate.co.nz';

    public $SDNCode;
    public $FTP;
    public $savePath = '/public/listings/';
    public $imagesPath = '/app/public/images/';

    // Firebase info    
    private $firebase;
    private $companyID = '-KF11_BPL1Nv0ET2RVq5';
    private $officeID = '-KG9hF_qHcKRnvsGgxrb';
    private $firebaseURL = '';
    private $firebaseSecret = '';   

    // Realestate.co.nz info
    private $dataProvider = 'LATRINITY1';
    private $listUserID = '20044353';
    private $listOfficeID = '5872807';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(SDNCode $SDNCode)
    {
        parent::__construct();

        if(App::environment('production')) {
            $this->firebaseURL = 'https://zara-crm.firebaseio.com/';
            $this->firebaseSecret = 'vnsqDY1uhXRarLq3zF5fEvM6n0K61ImXhl8oYnyz'; 
        } else {
            $this->firebaseURL = 'https://staging-zara-crm.firebaseio.com/';
            $this->firebaseSecret = 'YbXzF1ShURSy3NLrbXMBbMGZSu1RawsS6qssRymN'; 
        }

        $this->firebase = new \Firebase\FirebaseLib($this->firebaseURL, $this->firebaseSecret);
        $this->firebase->setBaseURI($this->firebaseURL);  
        $this->SDNCode = $SDNCode;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $workflowItems = $this->firebase->get('/workflow-items/' . $this->companyID . '/' . $this->officeID . '/');

        $lines = array();
        $listingCount = 0;

        foreach (json_decode($workflowItems, true) as $ID => $value) {
            $withdraw = (empty($value['stage']) || $value['stage'] == 'sold' || in_array($value['status'], array('Archived', 'Withdrawn', 'On Hold', 'Trashed', 'Deleted', 'Completed'))) ? true : false;
            if(isset($value['attributes']) && !$withdraw && isset($value['attributes']['listOnRealestateCoNz']) && isset($value['attributes']['realestateListingClass']) && isset($value['attributes']['realestateSaleType'])) {
          
                $listingID = str_replace("-", "", $value['listingID']);
                $listingCount++;
                // Get the property for the listing 
                foreach ($value['properties'] as $propertyID => $prop) {
                    $property = $this->firebase->get('/properties/' . $this->companyID . '/' . $this->officeID . '/' . $propertyID);
                }

                // Load the agents for the listing
                $agents = array();                
                foreach ($value['agents'] as $agentID => $ag) {
                    $agent = $this->firebase->get('/people/' . $this->companyID . '/' . $this->officeID . '/' . $agentID);
                    $agent = json_decode($agent, true);
                    if(isset($agent['agentAttributes']['reaaNumber'])) {
                       $agents[] = $agent['agentAttributes']['reaaNumber']; 
                    }
                }
                $value['agents'] = $agents;                

                $property = json_decode($property, true);
                $property['ID'] = $propertyID;
                $listing = $this->mapFields($listingID, $value, $property);
                $lines[]= implode("|", array_values($listing));

                // Update the timestamp for last synced with Realestate.co.nz
                $this->firebase->set('/workflow-items/' . $this->companyID  . '/' . $this->officeID. '/' . $ID . '/attributes/realestateLastSynced', date('Y-m-d h:i:s'));
            }
        }

        $fileOutput = '';
        foreach ($lines as $line) {
            $fileOutput .= $line . "\n";
        }
        
        if($listingCount > 0) {
          
            Storage::put($this->savePath . $this->dataProvider . '.DAT', $fileOutput);
           // Storage::put('/public/tests/'. time() . 'RENZ.DAT', $fileOutput);
            $file = $this->zipFiles();
            $this->ftpFile($file);
            $this->cleanup();            
        }

    }

    public function ftpFile($file) {
        
        $uploadedFile = str_replace(storage_path() . '/app/public/listings/', '', $file);
        $upload = FTP::connection('connection1')->uploadFile($file, $uploadedFile);
        //$upload2 = FTP::connection('connection2')->uploadFile($file, '/uploads/' . $uploadedFile);
        //$test = FTP::connection()->delete($uploadedFile);
        // $listing = FTP::connection('connection1')->getDirListing();
        // print_r($listing);
        FTP::disconnect('connection1');
        return $upload;
    }

    public function getSDNCode($region, $city, $suburb) {
        // First try to be specific with region, city and suburb
        $result = $this->SDNCode->where('region', $region)->where('district', $city)->where('suburb', $suburb)->first();
        // If that fails then try region and suburb
        if(!$result) {
          $result = $this->SDNCode->where('region', $region)->where('suburb', $suburb)->first();  
        }
        // If that fails then try just the suburb - if that fails then the code will be blank
        if(!$result) {
          $result = $this->SDNCode->where('suburb', $suburb)->first();  
        }
        if($result) {
            return $result->SDN;
        } else {
            return '';
        }
    }

    public function checkIfUploadImages($property) {
        $upload = false;

        if(isset($property['galleryImages'])) {
            foreach ($property['galleryImages'] as $key => $value) {
                if(isset($value['marketing']) && !isset($property['galleryImages'][$key]['realestateSynced'])) {
                   $upload = true;              
               }
            } 
        }  
     
        return $upload;     
    }

    public function createTempImages($listingID, $property) {
        $imageCount = 0;
        if($this->checkIfUploadImages($property)) {
            if(isset($property['galleryImages'])) {
                foreach ($property['galleryImages'] as $key => $value) {
                    if(isset($value['marketing']) && File::exists(storage_path() . $this->imagesPath . $value['large'])) {
                       $imageCount++; 
                       File::copy(storage_path() . $this->imagesPath . $value['large'], storage_path() . '/app/public/listings/' . $listingID . '-' . $value['order'] . '.jpg');
                       $this->firebase->set('/properties/' . $this->companyID . '/' . $this->officeID . '/' . $property['ID'] . '/galleryImages/' . $key . '/realestateSynced', date('Y-m-d h:m:s'));                       
                   }
                } 
            }            
        }
        return $imageCount;
    }

    public function zipFiles() {
        $zipper = new \Chumper\Zipper\Zipper;
        $zipPath = storage_path() . '/app/public/listings/';
        $zipFilePath = $zipPath . $this->dataProvider . '_' . date('Ymd') . '.zip';
        $files = glob($zipPath . '*');
        $zipper->make($zipFilePath)->add($files);
        return $zipFilePath;
    }

    public function cleanup() {
        $cleanupPath = storage_path() . '/app/public/listings/';
        $files = File::allFiles($cleanupPath);
        foreach ($files as $file)
        {
            File::delete($file);
        }
    }

    public function mapFields($listingID, $workflowItem, $property) {

        $description = urlencode($workflowItem['attributes']['advertisingCopy']);
        $description = urldecode(str_replace("%0A", '<br>', $description));
        $workflowItem['attributes']['price'] = (isset($workflowItem['attributes']['price'])) ? str_replace(",", "", $workflowItem['attributes']['price']) : '';
        $ensuites = (isset($property['attributes']['ensuites']) && intval($property['attributes']['ensuites']) > 0) ? $property['attributes']['ensuites'] : 0;
        if(isset($workflowItem['attributes']['realestateSDNCode']) && $workflowItem['attributes']['realestateSDNCode'] > 0) {
            $sdn = $workflowItem['attributes']['realestateSDNCode'];
        } else {
            $sdn = $this->getSDNCode($property['attributes']['region'], $property['attributes']['city'], $property['attributes']['suburb']);
        }

        // Handle parking 
        if(!empty($property['attributes']['garages']) || !empty($property['attributes']['carports']) || !empty($property['attributes']['offStreet'])) {
            $parking = 0;
            if(!empty($property['attributes']['garages'])) {
                $parking = $parking + intval($property['attributes']['garages']);
            }
            if(!empty($property['attributes']['carports'])) {
                $parking = $parking + intval($property['attributes']['carports']);
            }
            if(!empty($property['attributes']['offStreet'])) {
                $parking = $parking + intval($property['attributes']['offStreet']);
            }
        } else {
            $parking = (!empty($property['attributes']['parkingNumCars'])) ? $property['attributes']['parkingNumCars'] : '';
        }

        $fields = array(
            'ListingType' => $workflowItem['attributes']['realestateListingCode'],
            'DataProvider' => $this->dataProvider,
            'ListingNumber' => $listingID,
            'PhotoFlag' => $this->createTempImages($listingID, $property), // Number of photo files provided
            'DateEntered' => date('dmY', strtotime($workflowItem['created'])),
            'DateofLastChange' => date('dmY'),
            'VersionNumber' => '2',
            'ListUserID' => (count($workflowItem['agents']) > 0) ? $workflowItem['agents'][0] : $this->listUserID,
            'ListOfficeID' => $this->listOfficeID,
            'ListCoUserID' => (count($workflowItem['agents']) > 1) ? $workflowItem['agents'][1] : '',
            'ListCoOfficeID' => (count($workflowItem['agents']) > 1) ? $this->listOfficeID : '',
            'ListingClassCode' => $workflowItem['attributes']['realestateListingClass'],
            'SDNID' => $sdn,
            'PublishSuburb' => $property['attributes']['suburb'],
            'StreetNumber' => (!empty($property['attributes']['streetNumber'])) ? $property['attributes']['streetNumber'] : '',
            'StreetName' => $property['attributes']['streetName'],
            'SaleType' => $workflowItem['attributes']['realestateSaleType'],
            'PricingCode' => $workflowItem['attributes']['realestatePricingCode'],
            'Price' => str_replace("$", "", $workflowItem['attributes']['price']),
            'PriceMin' => (!empty($workflowItem['attributes']['minPrice'])) ? str_replace("$", "", str_replace(",", "", $workflowItem['attributes']['minPrice'])) : '',
            'PriceMax' => (!empty($workflowItem['attributes']['maxPrice'])) ? str_replace("$", "", str_replace(",", "", $workflowItem['attributes']['maxPrice'])) : '',
            'DateofAuction' => (!empty($workflowItem['attributes']['auctionDate'])) ? date('dmY', strtotime($workflowItem['attributes']['auctionDate'])) : '',
            'TimeofAuction' => (!empty($workflowItem['attributes']['auctionTime'])) ? date('H:i', strtotime($workflowItem['attributes']['auctionTime'])) : '',
            'PubAddrWeb' => '1',
            'FloorAreaSqM' => (!empty($property['attributes']['floorArea'])) ? $property['attributes']['floorArea']  : '',
            'LandAreaSqM' => (!empty($property['attributes']['landArea'])) ? $property['attributes']['landArea']  : '',
            'LandAreaSqHA' => (!empty($property['attributes']['landAreaHectares'])) ? $property['attributes']['landAreaHectares']  : '',
            'YearBuilt' => (!empty($property['attributes']['yearBuilt'])) ? $property['attributes']['yearBuilt']  : '',
            'NewConstruction' => (!empty($property['attributes']['newConstruction'])) ? $property['attributes']['newConstruction']  : '',
            'NoofStories' => (!empty($property['attributes']['floors'])) ? $property['attributes']['floors']  : '1',
            'UnitNumber' => (!empty($property['attributes']['unitNumber'])) ? $property['attributes']['unitNumber']  : '',
            'NoofBedrooms' => (!empty($property['attributes']['bedrooms'])) ? $property['attributes']['bedrooms']  : '',
            'NoofFullBathrms' => (!empty($property['attributes']['bathrooms'])) ? $property['attributes']['bathrooms'] : '',
            'NoofCars' => $parking,
            'NoofEnsuites' => (!empty($property['attributes']['ensuites'])) ? $property['attributes']['ensuites']  : '',
            'Header' => $workflowItem['attributes']['advertisingTitle'],
            'Description' => $description
        );

        for ($i=1; $i <= 35 ; $i++) { 
            $fields['Features' . $i] = '';
        }
        $videoID = (!empty($workflowItem['attributes']['youtubeVideoId'])) ? $workflowItem['attributes']['youtubeVideoId'] : '';
        $fields['Video URL'] = ($videoID != '') ? 'www.youtube.com/watch?v=' . $videoID : '';
        $fields['Additional Website'] = 'www.trinitynetwork.co.nz/property/' . str_replace('TTN-', '', $workflowItem['listingID']);

        return $fields;        
    }

}
