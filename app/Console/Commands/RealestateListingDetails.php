<?php

namespace ZaraServer\Console\Commands;

use Illuminate\Support\Facades\App;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Nathanmac\Utilities\Parser\Facades\Parser;
use Firebase\FirebaseLib;
use ZaraServer\RealestateAPI;

class RealestateListingDetails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zara:realestatelistingdetails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets the listing details as posted to realestate.co.nz';

    // Firebase info    
    private $firebase;
    private $companyID = '-KF11_BPL1Nv0ET2RVq5';
    private $officeID = '-KG9hF_qHcKRnvsGgxrb';
    private $firebaseURL = '';
    private $firebaseSecret = '';   

    // RENZ
    private $realestateAPI;
    private $listOfficeID = '5872807';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RealestateAPI $realestateAPI)
    {
        parent::__construct();

        if(App::environment('production')) {
            $this->firebaseURL = 'https://zara-crm.firebaseio.com/';
            $this->firebaseSecret = 'vnsqDY1uhXRarLq3zF5fEvM6n0K61ImXhl8oYnyz'; 
        } else {
            $this->firebaseURL = 'https://staging-zara-crm.firebaseio.com/';
            $this->firebaseSecret = 'ZXvAkAonaR9U9msaip0c7vbvQ0ofVfQKOqRCnKCp'; 
        }

        $this->firebase = new \Firebase\FirebaseLib($this->firebaseURL, $this->firebaseSecret);
        $this->firebase->setBaseURI($this->firebaseURL);  
        $this->realestateAPI = $realestateAPI;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $workflowItems = $this->firebase->get('/workflow-items/' . $this->companyID . '/' . $this->officeID . '/');

        foreach (json_decode($workflowItems, true) as $ID => $value) {
            $withdraw = (isset($value['stage']) && $value['stage'] == 'sold' || in_array($value['status'], array('Archived', 'Withdrawn', 'On Hold', 'Trashed', 'Deleted', 'Completed'))) ? true : false;
            if(isset($value['attributes']) && !$withdraw && isset($value['attributes']['listOnRealestateCoNz']) && isset($value['attributes']['realestateListingClass']) && (!isset($value['realestateListingID']) || $value['realestateListingID'] == '')) {
          
                $listingID = str_replace("-", "", $value['listingID']);
                $results = $this->realestateAPI->perform_http_request('listings/', 'GET', array('listing_no' => $listingID));
                $listing = (is_array($results)) ? json_decode($results[0]) : false;
                if($listing) {
                    $realestateID = ( is_array($listing->listings) && isset($listing->listings[0]->id)) ? $listing->listings[0]->id : false;
                    echo $realestateID;
                    if($realestateID) {
                        // Update the timestamp for last synced with Realestate.co.nz
                        $this->firebase->set('/workflow-items/' . $this->companyID . '/' . $this->officeID . '/' . $ID . '/realestateListingID', $realestateID);
                    }
                }
            }
        }
    }
}
