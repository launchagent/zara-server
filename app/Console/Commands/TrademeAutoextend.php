<?php

namespace ZaraServer\Console\Commands;

use Illuminate\Support\Facades\App;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Nathanmac\Utilities\Parser\Facades\Parser;
use Firebase\FirebaseLib;
use ZaraServer\TradeMe;

class TrademeAutoextend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zara:trademeautoextend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This runs the autoextend functionality for Trade Me';

    // Firebase info    
    private $firebase;
    private $companyID = '-KF11_BPL1Nv0ET2RVq5';
    private $firebaseURL = '';
    private $firebaseSecret = '';   

    private $trademe;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        if(App::environment('production')) {
            $this->firebaseURL = 'https://zara-crm.firebaseio.com/';
            $this->firebaseSecret = 'vnsqDY1uhXRarLq3zF5fEvM6n0K61ImXhl8oYnyz'; 
        } else {
            $this->firebaseURL = 'https://staging-zara-crm.firebaseio.com/';
            $this->firebaseSecret = 'ZXvAkAonaR9U9msaip0c7vbvQ0ofVfQKOqRCnKCp'; 
        }

        $this->firebase = new \Firebase\FirebaseLib($this->firebaseURL, $this->firebaseSecret);
        $this->firebase->setBaseURI($this->firebaseURL);  
        $this->trademe = new TradeMe();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $workflowItems = $this->firebase->get('/workflow-items/' . $this->companyID . '/');
        $workflowItems = json_decode($workflowItems, true);
        if(is_array($workflowItems)) {
         
            foreach ($workflowItems as $ID => $value) {
                if(isset($value['attributes']) && $value['stage'] != 'sold' && !empty($value['trademeListingID']) && isset($value['attributes']['autoExtendTrademe']) && isset($value['attributes']['autoExtendTrademe']) == true ) {           
                    $res = $this->trademe->perform_http_request('/Listings/' . $value['trademeListingID'] . '.xml', 'GET');
                    $listing = Parser::xml($res[0]);
                    $date = date_create(date("Y-m-d H:i:s", strtotime($listing['EndDate'])));
                    $nowDate = date_create(date("Y-m-d H:i:s", time()));
                    $nowDateTime = date_timezone_set($nowDate, timezone_open('UTC'));
                    if($nowDateTime > $date) {
                        $res1 = $this->trademe->perform_http_request('/Selling/Extend.xml', 'POST', $this->trademe->extend_listing_data($value['trademeListingID']));
                        $listing1 = Parser::xml($res1[0]);
                        $this->firebase->set('/workflow-items/' . $this->companyID . '/' . $ID . '/attributes/trademeDuration', 'UntilWithdrawn');
                    }
                }
            }
        }
    }



}
