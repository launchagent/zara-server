<?php

namespace ZaraServer\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Nathanmac\Utilities\Parser\Facades\Parser;
use Firebase\FirebaseLib;
use Anchu\Ftp\Facades\Ftp as FTP;


class RealestateOpenhomes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zara:realestateopenhomes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This runs the open home integration for realestate.co.nz';

    public $SDNCode;
    public $FTP;
    public $savePath = '/public/openhomes/';
    public $imagesPath = '/app/public/images/';
    public $mode = 'LIVE';

    // Firebase info    
    private $firebase;
    private $companyID = '-KF11_BPL1Nv0ET2RVq5';
    private $officeID = '-KG9hF_qHcKRnvsGgxrb';
    private $firebaseURL = 'https://zara-crm.firebaseio.com/';
    private $firebaseSecret = 'vnsqDY1uhXRarLq3zF5fEvM6n0K61ImXhl8oYnyz';   

    // Realestate.co.nz info
    private $dataProvider = 'LATRINITY1';
    private $listUserID = '20044353';
    private $listOfficeID = '5872807';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->firebase = new \Firebase\FirebaseLib($this->firebaseURL, $this->firebaseSecret);
        $this->firebase->setBaseURI($this->firebaseURL);  
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $workflowItems = $this->firebase->get('/workflow-items/' . $this->companyID . '/' . $this->officeID . '/');
        $lines = array();
        $listingCount = 0;
        $openHomeCount = 0;
        $lines[] = $this->getOpenhomeHeader();

        foreach (json_decode($workflowItems, true) as $ID => $value) {
            if(isset($value['attributes']) && isset($value['attributes']['listOnRealestateCoNz']) && isset($value['attributes']['realestateListingClass'])) {
                $listingID = str_replace("-", "", $value['listingID']);
                $listingCount++;

                $dont_show_statuses = array('Archived', 'Withdrawn', 'On Hold', 'Trashed', 'Deleted', 'Completed');
                if(isset($value['openHomes']) && in_array($value['stage'], array('listing', 'contract')) && !in_array($value['status'], $dont_show_statuses) ) {        
                    foreach ($value['openHomes'] as $key => $openHome) {
                        $lines[] = $this->mapFields($listingID, $openHome);
                        $openHomeCount++;                            
                    }                     
                }                
            }
        }

        $lines[] = $this->getOpenhomeFooter($openHomeCount);  

        $fileOutput = '';
        foreach ($lines as $line) {
            $fileOutput .= $line . "\n";
        }
        
        if($openHomeCount > 0) {
            $modeFlag = ($this->mode == 'TEST') ? 'T' : 'L';
            $sequenceNum = $this->firebase->get('/settings/' . $this->companyID . '/' . $this->officeID . '/openhomeSequence');
            $sequenceNum = intval($sequenceNum) + 1; 
            $file = storage_path() . '/app/public/openhomes/' . $this->dataProvider . '_' . $modeFlag . '_OPENHOME_000' . $sequenceNum . '.CSV';              
            Storage::put('/public/openhomes/' . $this->dataProvider . '_' . $modeFlag . '_OPENHOME_000' . $sequenceNum . '.CSV', $fileOutput);
            $this->firebase->set('/settings/' . $this->companyID . '/' . $this->officeID . '/openhomeSequence', $sequenceNum);
            $this->ftpFile($file);
            $this->cleanup();            
        }

    }

    public function ftpFile($file) {
        
        $uploadedFile = str_replace(storage_path() . '/app/public/openhomes/', '', $file);
        $upload = FTP::connection('connection1')->uploadFile($file, $uploadedFile);
        //$test = FTP::connection()->delete($uploadedFile);
        //$listing = FTP::connection()->getDirListing();
        FTP::disconnect('connection1');
        return $upload;
    }



    public function zipFiles() {
        $zipper = new \Chumper\Zipper\Zipper;
        $zipPath = storage_path() . '/app/public/openhomes/';
        $zipFilePath = $zipPath . $this->dataProvider . '_' . date('Ymd') . '.zip';
        $files = glob($zipPath . '*');
        $zipper->make($zipFilePath)->add($files);
        return $zipFilePath;
    }

    public function cleanup() {
        $cleanupPath = storage_path() . '/app/public/openhomes/';
        $files = File::allFiles($cleanupPath);
        foreach ($files as $file)
        {
            File::delete($file);
        }
    }

    public function mapFields($listingID, $openHome) {

        $fields = array(
            'RecordType'    => '"D"',
            'ListingNum'    => '"' . $listingID . '"',
            'OfficeCode'    => '"' . $this->listOfficeID . '"',
            'OpenDate'      => '"' . substr($openHome['start'], 0, 10) . '"',
            'StartTime'     => '"' . substr($openHome['start'], 11, 5) . '"',
            'EndTime'       => '"' . substr($openHome['end'], 11, 5) . '"',
            'ExpiryFlag'    => '"0"',
            'Notes'         => "",
            'CreateDate'    => '"' . date('Y-m-d') . '"'
        );
        return implode(",", array_values($fields));
    }

    public function getOpenhomeHeader() {
        $fields = array(
            'RecordType' => '"H"', // Header Record type
            'FileCode'  => '"' . $this->dataProvider . '"',
            'FileType'  => '"OPENHOME"',
            'LoadType'  => '"F"', // P for partial or F for full flush
            'Version'   => '"1.0"',
            'TestOrLive' => ($this->mode == 'TEST') ? '"T"' : '"L"',
            'EmailAddress' => '"jon@launchagent.co.nz"', // Notification email
            'DateCreated' => '"' . date('Y-m-d') . '"',
            'TimeCreated' => '"' . date('H:i') . '"'
        ); 
        return implode(",", array_values($fields));       
    }

    public function getOpenhomeFooter($numRecords) {
        $fields = array(
            'RecordType' => '"F"', // Header Record type
            'RecordCount'  => '"' . $numRecords . '"',
        ); 
        return implode(",", array_values($fields));      
    }

    public function checkIfUpload($ID, $workflowItem) {
        $upload = false;
        if(isset($workflowItem['openHomes'])) {
            foreach ($workflowItem['openHomes'] as $key => $value) {
                if(!isset($value['realestateSynced']) || $value['realestateSynced'] == '') {
                    $upload = true; 
                    $this->firebase->set('/workflow-items/' . $this->companyID . '/' . $this->officeID . '/' . $ID . '/openHomes/' . $key . '/realestateSynced', date('Y-m-d h:m:s'));         
               }
            } 
        }   
        return $upload;     
    }

}
