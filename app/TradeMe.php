<?php

namespace ZaraServer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class TradeMe extends Model
{

  private $token = "";
  private $secret = "";

  private $consumer_key    = "";
  private $consumer_secret = "";

  private $consumer_key_live    = "7F219ED47589F258B7554E7AB704AD49";
  private $consumer_secret_live = "76858B8CDD8715E16A693EED95F32D06";

  private $sandbox_consumer_key    = "8E325535B9180AE21293367943F125CB";
  private $sandbox_consumer_secret = "49CE034975CE6EFEA77011E63EEAC5CB";
  
  public  $domain          = '';
  private $liveDomain      = 'https://api.trademe.co.nz/v1';
  private $sandboxDomain   = 'https://api.tmsandbox.co.nz/v1';
                              
  // The callback url is the FULL url to callback.php (in this example!)
  //   if the auto code doesn't work for you, set this to the url to /example/callback.php 
  private $callback_url = '';
  public $tm;	

	public function __construct() {
    if(App::environment('production')) {
          $this->domain = $this->liveDomain;
          $this->consumer_key = $this->consumer_key_live;
          $this->consumer_secret = $this->consumer_secret_live;
          $this->token = '49ADFF967FF03CBEBCBF9C314097A65A';
          $this->secret = 'DBAB49B3E46A674D1B00EE0785495F21';
    } else {
      $this->domain = $this->sandboxDomain;
      $this->consumer_key = $this->sandbox_consumer_key;
      $this->consumer_secret = $this->sandbox_consumer_secret;
      $this->token = 'BD3A94C23EFA4A6E6ECEB6C173088BE4';
      $this->secret = '8323DDD784E2CFC4759B002CC1B0B385';
    }
	}
 

    public function perform_http_request($endpoint, $Method = 'GET',  $FormPostData = NULL)
    {  
      
      $url = $this->domain . $endpoint;
      $curl = curl_init($url);  

      	$header = array();
		//$header[] = 'Content-length: 0';
		$header[] = 'Content-type: application/xml';
		$header[] = 'Authorization: OAuth oauth_consumer_key="' . $this->consumer_key . '", oauth_token="' . $this->token . '", oauth_signature_method="PLAINTEXT", oauth_signature="' . $this->consumer_secret . '&' . $this->secret . '"';          
      
      $curlOptions = array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT        => 30,
        CURLOPT_HEADER         => FALSE,
        CURLOPT_HTTPHEADER     => $header,
      );

      if(isset($_SERVER['HTTP_HOST'])) {
        $curlOptions[CURLOPT_USERAGENT] = 'OAuth (' . $_SERVER['HTTP_HOST'] . ')';
      }

      curl_setopt_array($curl, $curlOptions);

      //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      if(defined('CURLINFO_HEADER_OUT'))
      {
        curl_setopt($curl, CURLINFO_HEADER_OUT, FALSE);
      }
      
      // Do anything for the specific method
      switch(strtoupper($Method))
      {
        case 'GET':
        {        
          
        }
        break;
        
        case 'POST':
        {  
          curl_setopt_array($curl, array(
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $FormPostData
          ));
        }
        break;
        
        case 'DELETE':
        {
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }         
        break;
      }
            
      // Do the request and grab the response and some info about it which may be useful
      $response = curl_exec($curl);
      $info     = curl_getinfo($curl);
      if(!defined('CURLINFO_HEADER_OUT'))
      {
        $info['request_header'] = implode("\r\n", $Headers);
      }
      
      // Ditch curl and return the response and info
      curl_close($curl);      
      return array($response);
    }

    /*
      --- Generate the photo data for trade me listings ---
    */
    public function create_photo_data($img) {
      $photoXML = '<PhotoUploadRequest xmlns="http://api.trademe.co.nz/v1">';
      $photoXML .= '<PhotoData>' . base64_encode($img) . '</PhotoData>';
      $photoXML .= '<FileName>propertyImage.jpg</FileName>';
      $photoXML .= '<FileType>JPEG</FileType>';
      $photoXML .= '</PhotoUploadRequest>';
      return $photoXML;
    }

    /*
      --- Generate the xml post data for a new trade me listing ---
    */
    public function create_listing_data($data) {
      $listingXML = '<ListingRequest xmlns="http://api.trademe.co.nz/v1">';
      $listingXML .= $this->generate_xml_data($data);
      $listingXML .= '</ListingRequest>';
      return $listingXML;
    }

    /*
      --- Generate the xml post data for extending trade me listing ---
    */
    public function extend_listing_data($listingId) {
      $listingXML = '<ExtendClassifiedRequest xmlns="http://api.trademe.co.nz/v1">';
      $listingXML .= $this->xml_tag('ListingId', $listingId);
      $listingXML .= '<ReturnListingDetails>true</ReturnListingDetails>';
      $listingXML .= '</ExtendClassifiedRequest>';
      return $listingXML;
    }

    /*
      --- Generate the xml post data for boosting the trade me listing ---
    */
    public function boost_listing_data($data) {
      $listingXML = '<ListingTemporalExtraPromotionRequest xmlns="http://api.trademe.co.nz/v1">';
      $listingXML .= $this->xml_tag('ListingId', $data['trademeListingID']);
      $listingXML .= '<Extra>Boost</Extra>';
      $listingXML .= '</ListingTemporalExtraPromotionRequest>';
      return $listingXML;
    }

    /*
      --- Generate the xml post data for an existing trade me listing ---
    */
    public function edit_listing_data($data) {
      $listingXML = '<EditListingRequest xmlns="http://api.trademe.co.nz/v1">';
      $listingXML .= $this->generate_xml_data($data);
      $listingXML .= '<ListingId>' . $data['trademeListingID'] . '</ListingId>';
      $listingXML .= '</EditListingRequest>';
      return $listingXML;
    }

    /*
      --- Generate the xml post data for an withdrawing a trade me listing ---
    */
    public function withdraw_listing_data($data) {
      $listingXML = '<WithdrawRequest xmlns="http://api.trademe.co.nz/v1">';
      $listingXML .= '<ListingId>' . $data['trademeListingID'] . '</ListingId>';
      $listingXML .= '<ReturnListingDetails>false</ReturnListingDetails>';
      $listingXML .= '<Type>ListingWasSold</Type>';
      $listingXML .= '<Reason>' . $data['reason'] . '</Reason>';
      $listingXML .= '<SalePrice>0.0</SalePrice>';
      $listingXML .= '</WithdrawRequest>';
      return $listingXML;
    }

    /*
      --- Generate the xml data ---
    */
    public function generate_xml_data($data) {
      // Remove any url encoding
      foreach ($data as $key => $value) {
        if(!is_array($value) && $key != 'description') {
          $data[$key] = urldecode($value);  
        }
      }

      $listingXML = '';
      $listingXML .= $this->xml_tag('Category', $data['trademeCategoryID']);
      $listingXML .= $this->xml_tag('Title', $data['title']);
      // Generate description paragraphs %0A%0A is 2 new lines
      $description = '';
      $descriptionParts = explode("\n\n", str_replace("&", "&amp;", $data['description']));
      foreach ($descriptionParts as $part) {
        $description .= $this->xml_tag('Paragraph', $part . "\n");
      }
      $listingXML .= $this->xml_tag('Description', $description);

      // if($data['endDate'] != '') {
      //   $listingXML .= $this->xml_tag('Duration', 'EndDate'); 
      //   $listingXML .= $this->xml_tag('EndDateTime', str_replace(' ', 'T', date('Y-m-d H:i:s', strtotime($data['endDate'])) . 'Z'));
      // } else {
      //   $listingXML .= $this->xml_tag('Duration', 'UntilWithdrawn'); 
      // }
      // if(isset($data['trademeDuration']) && $data['trademeDuration'] != '') {
      //   $listingXML .= $this->xml_tag('Duration', $data['trademeDuration']);
      // } else {
        
      // }
      
      $listingXML .= $this->xml_tag('Duration', 'UntilWithdrawn'); 
      $listingXML .= $this->xml_tag('IsClassified', 'true');

      $openHomes = array();
 
      foreach ($data['openHomes'] as $key => $openHome) {
        if(strtotime($this->getFormattedUTC($openHome['start'])) > time()) {
          $openHomes[] = array('Start' => $this->getFormattedUTC($openHome['start']), 'End' => $this->getFormattedUTC($openHome['end']));  
        }
      }
      if(count($openHomes) > 0) {
        $listingXML .= $this->openhomes_tags($openHomes);  
      }

      if(isset($data['trademeFeatured']) && $data['trademeFeatured'] == true) {
        $listingXML .= $this->xml_tag('IsFeatured', 'true');
      }

      if($data['propertyType'] == 'Lifestyle - Dwelling') {
        $data['propertyType'] = 'Dwelling';
      } elseif($data['propertyType'] == 'Lifestyle - Bare land') {
        $data['propertyType'] = 'Bareland';
      }

      if(!empty($data['garages']) || !empty($data['carports']) || !empty($data['offStreet'])) {
        $parking = array();
        if(!empty($data['garages'])) {
          $parking[] = 'Garages: ' . $data['garages'];
        }
        if(!empty($data['carports'])) {
          $parking[] = 'Carports: ' . $data['carports']; 
        }
        if(!empty($data['offStreet'])) {
          $parking[] = 'Off street: ' . $data['offStreet'];
        } 
        $parking = implode(', ', $parking);
      } else {
        $parking = (!empty($data['parking'])) ? $data['parking'] : '';
      }



      $attributes = array(
          array('Name' => 'PropertyType', 'Value' => $data['propertyType']),
          array('Name' => 'PropertyPriceType', 'Value'=> $this->getPricingType($data['saleType'])),
          array('Name' => 'Region', 'Value'=> $data['regionID']),
          array('Name' => 'District', 'Value'=> $data['regionID']),
          array('Name' => 'Suburb', 'Value'=> $data['suburbID']),
          array('Name' => 'PropertyStreetNumber', 'Value'=> $data['streetNumber']),
          array('Name' => 'PropertyUnit', 'Value'=> $data['unitNumber']),
          array('Name' => 'PropertyStreetName', 'Value'=> $data['streetName']),
          array('Name' => 'ParkingOrGaraging', 'Value'=> $parking)
      );

      $isSection = ($data['trademeCategoryID'] == 3400) ? true : false;

      if(isset($data['bedrooms']) && intval($data['bedrooms']) > 0 && !$isSection) {
        $attributes[] = array('Name' => 'Bedrooms', 'Value'=> $data['bedrooms']);
      }


      if(isset($data['bathrooms']) && intval($data['bathrooms']) > 0 && !$isSection) {
        $data['bathrooms'] = (!empty($data['ensuites'])) ? intval($data['bathrooms']) + intval($data['ensuites']) : $data['bathrooms'];
        $attributes[] = array('Name' => 'Bathrooms', 'Value'=> $data['bathrooms']);
      }

      if(isset($data['landArea']) && $data['landArea'] != '') {
        if(intval($data['landArea']) >= 10000 && $data['trademeCategoryID'] != '3399') {
          $landArea = round(intval($data['landArea']) / 10000, 2);
          $attributes[] = array('Name' => 'PropertyArea', 'Value'=> $landArea, 'Units' => 'hectares', 'Multiplier' => 1);
        } else {
          $attributes[] = array('Name' => 'LandArea', 'Value'=> $data['landArea']);
        }
      }

      if(isset($data['floorArea']) && intval($data['floorArea']) > 0 && !$isSection) {
        $attributes[] = array('Name' => 'FloorArea', 'Value'=> $data['floorArea']);
      }

      if(isset($data['localAmenities']) && $data['localAmenities'] != '') {
        $attributes[] = array('Name' => 'AmenitiesInTheArea', 'Value'=> $data['localAmenities']);
      }

      if(isset($data['smokeAlarm']) && $data['smokeAlarm'] == 'yes') {
        $attributes[] = array('Name' => 'HasSmokeAlarm', 'Value'=> 'true');
      }  

      if($this->getPricingType($data['saleType']) == 'Auction' || $this->getPricingType($data['saleType']) == 'Tender') {
        $attributes[] = array('Name' => 'PropertyPriceDate', 'Value'=> $data['priceDate'] . 'T00:00:00Z');
      }

      // Sale price
      if(isset($data['price']) && $data['price'] != '' && $data['price'] !== 0 && $data['price'] != 'By Negotiation') {

        $price = (is_string($data['price'])) ? str_replace('$', '', str_replace(',', '', $data['price'])) : $data['price'];
        $attributes[] = array('Name' => 'PropertyPricePrice', 'Value'=> number_format(floatval($price), 1, '.', ''));
      }

      // Expected sale price
      if(isset($data['expectedSalePrice']) && $data['expectedSalePrice'] != '') {

        $expectedPrice = (is_string($data['expectedSalePrice'])) ? str_replace('$', '', str_replace(',', '', $data['expectedSalePrice'])) : $data['expectedSalePrice'];
        $attributes[] = array('Name' => 'ExpectedSalePrice', 'Value'=> number_format(floatval($expectedPrice), 1, '.', ''));
      }

      if($data['cv'] > 0) {
        $attributes[] = array('Name' => 'RateableValue', 'Value'=> $data['cv']);
      }

      if(isset($data['trademePhotoIDs'])) {
        $photoIDs = $data['trademePhotoIDs'];
        $photoIDs = array_slice($photoIDs, 0, 19);
        if(count($data['trademePhotoIDs']) > 0) {
          $listingXML .= $this->photos_tags($photoIDs);
        }
      }

      $listingXML .= $this->attributes_tags($attributes); 

      $contacts = array();

      foreach ($data['contacts'] as $key => $contact) {
        $contactData = array('FullName' => $contact['name'], 'PhoneNumber' => $contact['phone'], 'AlternatePhoneNumber' => $contact['mobile'], 'EMail' => $contact['email'], 'AgentId' => $contact['agentID']);
          if(isset($contact['brandingImageID']) && $contact['brandingImageID'] != '') {
            $contactData['BrandingImageId'] = $contact['brandingImageID'];
          }
        $contacts[] = $contactData;
      }

      $listingXML .= $this->contacts_tags($contacts);

      if(!empty($data['latitude']) && !empty($data['longitude'])) {
        $listingXML .= $this->geographic_location_tags($data['latitude'], $data['longitude']);
      }

      $listingXML .= $this->xml_tag('ExternalReferenceId', $data['listingID']);
      $listingXML .= $this->xml_tag('AgencyReference', $data['listingID']);

      if(isset($data['youtubeVideoID']) && $data['youtubeVideoID'] != '') {
        $youtube = $this->xml_tag('YouTubeVideoKey', $data['youtubeVideoID']);
        $listingXML .= $this->xml_tag('EmbeddedContent', $youtube);
      }
   
      return $listingXML;
    }

    /*
      --- Generate an xml tag with content ---
    */
    public function xml_tag($tag, $content) {
      return '<' . $tag . '>' . $content . '</' . $tag . '>' . "\n";
    }

    /*
      --- Generate an attribute tag ---
    */
    public function attributes_tags($attributes) {
      $string = '<Attributes>';
      foreach($attributes as $attr) {
          $tagContent =  "\n" . $this->xml_tag('Name', $attr['Name']);
          $tagContent .= $this->xml_tag('Value', $attr['Value']);
          if(isset($attr['Units'])) {
            $tagContent .= '<Units><AttributeUnit>';
            $tagContent .= $this->xml_tag('Display', $attr['Units']);
            $tagContent .= $this->xml_tag('Multiplier', $attr['Multiplier']);
            $tagContent .= '</AttributeUnit></Units>';
          }
          $string .= $this->xml_tag('Attribute', $tagContent);          
      }
      $string .= '</Attributes>' . "\n";
      return $string;
    }

    /*
      --- Generate geographic location tags ---
    */
    public function geographic_location_tags($latitude, $longitude) {
      $string = '<GeographicLocation>';
      $string .= $this->xml_tag('Latitude', $latitude); 
      $string .= $this->xml_tag('Longitude', $longitude);
      $string .= '<Accuracy>Address</Accuracy>';          
      $string .= '</GeographicLocation>' . "\n";
      return $string;
    }

    /*
      --- Generate photo tags ---
    */
    public function photos_tags($photoIDs) {
      $string = '<PhotoIds>';
      foreach($photoIDs as $id) {
          $string .= $this->xml_tag('PhotoId', $id);          
      }
      $string .= '</PhotoIds>' . "\n";
      return $string;
    }

    /*
      --- Generate opem home tags ---
    */
    public function openhomes_tags($openHomes) {
      $string = '<OpenHomes>';
      foreach($openHomes as $attr) {
        $tagContent = $this->xml_tag('Start', $attr['Start']);
        $tagContent .= $this->xml_tag('End', $attr['End']);
        $string .= $this->xml_tag('OpenHome', $tagContent);
      }
      $string .= '</OpenHomes>' . "\n";
      return $string;
    }

    /*
      --- Generate contacts tags ---
    */
    public function contacts_tags($contacts) {
      $string = '<Contacts>';
      foreach($contacts as $attr) {
        $tagContent = '';
        foreach ($attr as $key => $value) {
          $tagContent .= $this->xml_tag($key, $value);
        }
        $string .= $this->xml_tag('Contact', $tagContent);
      }
      $string .= '</Contacts>' . "\n";
      return $string;
    }

    /*
      --- Map the pricing type to the trade pricing type ---
    */
    public function getPricingType($type) {
      $key = '';
      switch ($type) {
        case 'Asking Price':
          $key = 'AskingPrice';
          break;
        case 'Enquiries Over':
          $key = 'EnquiriesOver';
          break;  
        case 'By Negotiation':
          $key = 'PriceByNegotiation';
          break;  
        case 'Auction':
          $key = 'Auction';
          break;   
        case 'Tender':
          $key = 'Tender';
          break;  
      }
      return $key;
    }

// Trade me property is category 0350
    public function getCategoryMap() {
      return array(
        'Commercial' => array(
            'id' => 0100,
            'subcategories'   => array(
              'carparks'      => array('id' => 8946),
              'for-lease'     => array('id' => 5747),
              'for-sale'      => array('id' => 5746)
            )
          ),
        'Residential' => array(
            'id' => 3399,
            'subcategories' => array(
              'carparks'            => array('id' => 8945),
              'sections-for-sale'   => array('id' => 3400),
              'for-sale'            => array('id' => 3399),
              'lifestyle-property'  => array('id' => 9120),
              'to-rent'             => array('id' => 4233)
            )
        ),
        'Rural' => array('id' => 5745)
        );
    }

    public function getFormattedUTC($dateString) {
      $date = date_create($dateString, timezone_open('Pacific/Auckland'));

      $myDateTime = date_timezone_set($date, timezone_open('UTC'));
      return str_replace(" ", "T", date_format($myDateTime, 'Y-m-d H:i:s')) . 'Z';
    }

}
